//AppNavigation.js
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import Icon from '@expo/vector-icons/Ionicons';


import { createStackNavigator } from 'react-navigation-stack'
import Home from '../screens/Home'
import Home2 from '../screens/Home2'
import Home3 from '../screens/Home3'
import Home4 from '../screens/Home4'
import { createTabNavigator, createBottomTabNavigator } from 'react-navigation-tabs'
import { createDrawerNavigator } from 'react-navigation-drawer'


// const AppNavigation = createStackNavigator(
//   {
//     Home: { screen: Home }
//   },
//   {
//     initialRouteName: 'Home',
//     headerMode:'none'
//   }
// )

const HomeStack = createStackNavigator(
    {
      Home: { screen: Home }
    },
    {
      initialRouteName: 'Home',
      headerMode:'none'
    }
  )

const TabHomeNavigator = createBottomTabNavigator(
    {
    Home: HomeStack,
    Home2: Home2,
    Home3: Home3
    },
    {

    }
)


const DashboardStackNavigator = createStackNavigator(
    {
      DashboardTabNavigator: TabHomeNavigator
    },
    {
      defaultNavigationOptions: ({ navigation }) => {
        return {
          headerLeft: (
            <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
          )
        };
      }
    }
  );

const AppDrawerNavigator = createDrawerNavigator({
    Dashboard: {
      screen: DashboardStackNavigator,
    },
    Home4: {
        screen: Home4
      }
  });

// export default AppNavigation

// export default TabHomeNavigator

export default AppDrawerNavigator