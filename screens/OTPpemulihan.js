import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

export default class Verifikasi extends React.Component {
    goGantiPassword = () => this.props.navigation.navigate('GantiPassword')
    render() {
      return (
        <View style={styles.container}>
          <Text>Halaman Konfirmasi OTP Pemulihan Password</Text>
          <Button title='Go to Ganti Password Screen' onPress={this.goGantiPassword} />
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})