import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

export default class Verifikasi extends React.Component {
    goSignup = () => this.props.navigation.navigate('Signup')
    render() {
      return (
        <View style={styles.container}>
          <Text>Halaman Verifikasi</Text>
          <Button title='Go back to Signup screen' onPress={this.goSignup} />
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})