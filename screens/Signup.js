import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

export default class Signup extends React.Component {
    goVerifikasi = () => this.props.navigation.navigate('Verifikasi')
    goLogin = () => this.props.navigation.navigate('Login')
    render() {
      return (
        <View style={styles.container}>
          <Text>Halaman Signup</Text>
          <Button title='Go to Verifikasi' onPress={this.goVerifikasi} />
          <Button title='Go back to Login' onPress={this.goLogin} />
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})