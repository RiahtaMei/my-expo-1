import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

export default class Verifikasi extends React.Component {
    goOTPpemulihan = () => this.props.navigation.navigate('OTPpemulihan')
    render() {
      return (
        <View style={styles.container}>
          <Text>Halaman Lupa Kata Sandi</Text>
          <Button title='Go to konfirmasi OTP' onPress={this.goOTPpemulihan} />
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})