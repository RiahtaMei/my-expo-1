import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

export default class Verifikasi extends React.Component {
    goLogin = () => this.props.navigation.navigate('Login')
    render() {
      return (
        <View style={styles.container}>
          <Text>Halaman Ganti Kata Sandi</Text>
          <Button title='Go back to Login Screen' onPress={this.goLogin} />
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})